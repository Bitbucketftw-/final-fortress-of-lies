﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Data;
using Mono.Data.Sqlite;

public class HighscoreTable : MonoBehaviour
{

    private string connectionString;
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<HighscoreEntry> highscoreEntryList;
    private List<Transform> highscoreEntryTransformList;
    private TimeSpan timeScore;
    public TimerController timer;
    public PlayerPreferences prefencias;


    private void Awake() {
        connectionString = "URI=file:" + Application.dataPath + "/Database/HighscoresDB.sqlite";
        int lvIndex = SceneManager.GetActiveScene().buildIndex;
        float newTimeScore = timer.GetTimer();
        string playerName = prefencias.getPlayerName();
        InsertScore(lvIndex, newTimeScore, playerName);
        GetLevelScores(lvIndex);
    }

    
    public void InsertScore(int lvIndex, float timeScore, string name) {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString)) {

            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand()) {

                string sqlQuery = String.Format("INSERT INTO Highscores(LvIndex, Score, Name) VALUES(\"{0}\",\"{1}\",\"{2}\")", lvIndex, timeScore, name);
                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbConnection.Close();

                
            }
        }
    }

    public void GetLevelScores(int actualLv) {
        entryContainer = transform.Find("highscoreEntryContainer");
        entryTemplate = entryContainer.Find("highscoreEntryTemplate");
        entryTemplate.gameObject.SetActive(false);

        highscoreEntryList = new List<HighscoreEntry>();
        highscoreEntryList.Clear();


        using (IDbConnection dbConnection = new SqliteConnection(connectionString)) {
            
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
                
                string sqlQuery = "SELECT * FROM Highscores WHERE LvIndex = '"+actualLv+"'";
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader()) {

                    while (reader.Read()) {
                        highscoreEntryList.Add(new HighscoreEntry { lvIndex = reader.GetInt32(0), score = float.Parse(reader.GetString(1)), name = reader.GetString(2) });
                    }
                    

                    dbConnection.Close();
                    reader.Close();
                }
            }
        }

        //Ordena los scores de menor a mayor
        for (int i = 0; i < highscoreEntryList.Count; i++) {
            for (int j = i + 1; j < highscoreEntryList.Count; j++) {
                if (highscoreEntryList[j].score < highscoreEntryList[i].score) {
                    HighscoreEntry temp = highscoreEntryList[i];
                    highscoreEntryList[i] = highscoreEntryList[j];
                    highscoreEntryList[j] = temp;
                }
            }
        }

        //Crea entradas para los 10 mejores tiempos
        highscoreEntryTransformList = new List<Transform>();
        for (int i = 0; i < 10; i++) {
            HighscoreEntry highscoreEntry = highscoreEntryList[i];
            CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
        }
        
    }

    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList) {
        float templateHeight = 50f;
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;
        string rankString;
        switch (rank) {

            case 1: rankString = "1ST"; break;
            case 2: rankString = "2ND"; break;
            case 3: rankString = "3RD"; break;

            default:
                rankString = rank + "TH"; break;
        }

        entryTransform.Find("Position").GetComponent<Text>().text = rankString;

        timeScore = TimeSpan.FromSeconds(highscoreEntry.score);
        string timeScoreStr = "" + timeScore.ToString("mm':'ss'.'ff");
        entryTransform.Find("TimeScore").GetComponent<Text>().text = timeScoreStr;

        string name = highscoreEntry.name;
        entryTransform.Find("Name").GetComponent<Text>().text = name;

        transformList.Add(entryTransform);
    }

    private class HighscoreEntry {
        public int lvIndex;
        public float score;
        public string name;

    }
}
