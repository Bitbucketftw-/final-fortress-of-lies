﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class TimerController : MonoBehaviour
{
    public static TimerController instance;

    public TMP_Text timeCounter;

    private TimeSpan timePlaying;
    private bool timerGoing;

    private float elapsedTime;

    void Awake() {
        instance = this;
    }
    void Start()
    {
        timeCounter.text = "00:00.00";
        //timerGoing = false;
        timerGoing = true;
        elapsedTime = 0f;

        StartCoroutine(UpdateTimer());

    }

    void Update() {
        //Debug.Log(elapsedTime);
    }

    public float GetTimer() {
        //timePlaying = TimeSpan.FromSeconds(elapsedTime);
        //string timeScoreDB = elapsedTime.ToString("0.##");
        return elapsedTime;

    }

    public void EndTimer() {
        timerGoing = false;
    }

    private IEnumerator UpdateTimer() {
        while (timerGoing) {
            //Debug.Log("TimerFunciona");
            elapsedTime += Time.deltaTime;
            timePlaying = TimeSpan.FromSeconds(elapsedTime);
            string timePlayingStr = "" + timePlaying.ToString("mm':'ss'.'ff");
            timeCounter.text = timePlayingStr;

            yield return null;
        }
    }
    
}
