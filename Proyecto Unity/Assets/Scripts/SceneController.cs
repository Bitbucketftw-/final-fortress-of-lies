﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{

    public int enemiesRemaining = 2;
    public GameObject completeLevelUI;
    public GameObject deathMenuUI;

    private void Awake() {
        Time.timeScale = 1f;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        if (enemiesRemaining == 0) {
            LevelCompletePhase();
        }
        //Debug.Log(enemiesRemaining);
        
    }

    public void enemyDead() {
        enemiesRemaining--;
    }

    public void DeadPhase() {
        deathMenuUI.SetActive(true);
        Time.timeScale = 0f;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        GetComponent<TimerController>().EndTimer();
    }

    private void LevelCompletePhase() {
        //Debug.Log("YOU WIN");
        completeLevelUI.SetActive(true);
        GetComponent<TimerController>().EndTimer();
    }
}
