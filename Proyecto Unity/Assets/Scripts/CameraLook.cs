﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraLook : MonoBehaviour
{

    //Les asignamos objetos
    public Transform playerCam;
    public Transform orientation;

    //Sensibilidad y rotación
    private float xRotation;
    private float sensitivity;
    private float sensMultiplier = 1f;
    private float desiredX;
    public PlayerPreferences preferencias;


    private void Awake() {
        //DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //Se asegura de que el ratón se encuentre bloqueado en la pantalla
        Cursor.visible = false; 
    }

    // Update is called once per frame
    void Update()
    {
        sensitivity = preferencias.getSensivility();
        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.fixedDeltaTime * sensMultiplier * Time.timeScale;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.fixedDeltaTime * sensMultiplier * Time.timeScale;

        //Busca la rotación actual de la cámara del jugador
        Vector3 rot = playerCam.transform.localRotation.eulerAngles;
        desiredX = rot.y + mouseX;

        //Establecemos límite de rotación para la cámara en el eje x
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        //Realiza la rotación
        playerCam.transform.localRotation = Quaternion.Euler(xRotation, desiredX, 0);
        orientation.transform.localRotation = Quaternion.Euler(0, desiredX, 0);
    }

 
}
