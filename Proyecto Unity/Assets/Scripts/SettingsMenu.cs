﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{

    public AudioMixer audioMixer;
    public Slider sliderSensivity;
    public PlayerPreferences preferencias;

    void Start() {
        GetSensivility();
    }
    

    public void SetMasterVolume (float mVolume) {
        audioMixer.SetFloat("masterVolume", mVolume);

    }
    public void SetMusicVolume(float mVolume) {
        audioMixer.SetFloat("musicVolume", mVolume);

    }
    public void SetSFXVolume(float mVolume) {
        audioMixer.SetFloat("soundEffectVolume", mVolume);

    }

    public void SetSensivility(float sensiValue) {
        preferencias.setSensivility(sensiValue);
    }

    public void GetSensivility() {
        sliderSensivity.value = preferencias.getSensivility();
    }

    public void SetFullscreen (bool isFullscreen) {
        Screen.fullScreen = isFullscreen;
        Debug.Log("Fullscreen");
    }
}
