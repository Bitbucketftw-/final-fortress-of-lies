﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealthManagement : MonoBehaviour
{
    public SceneController sceneControl;
    public int health = 100;
    public TMP_Text healthText;

    void Start()
    {
        
    }

    void Update()
    {
        if(health <= 0) {
            sceneControl.DeadPhase();
        }
        //Debug.Log(health);
        healthText.text = "HP: " + health;
        
    }

    public void bulletDamage(int damage) {
        health -= damage;
    }

  
}
