﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Text enterName;
    public PlayerPreferences preferencias;

    void Start() {
        Time.timeScale = 1f;
        FindObjectOfType<AudioManager>().Play("MenuTheme");
    }

    public void ExitGame() {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void Stage01() {
        if(enterName.text != string.Empty) {
            string newName = enterName.text;
            preferencias.setPlayerName(newName);
            FindObjectOfType<AudioManager>().Stop("MenuTheme");
            FindObjectOfType<AudioManager>().Play("LevelTheme");
            SceneManager.LoadScene(1);
            
        }
        
    }
    public void Stage02() {
        if (enterName.text != string.Empty) {
            string newName = enterName.text;
            preferencias.setPlayerName(newName);
            FindObjectOfType<AudioManager>().Stop("MenuTheme");
            FindObjectOfType<AudioManager>().Play("LevelTheme");
            SceneManager.LoadScene(2);
        }
    }
    public void Stage03() {
        if (enterName.text != string.Empty) {
            string newName = enterName.text;
            preferencias.setPlayerName(newName);
            FindObjectOfType<AudioManager>().Stop("MenuTheme");
            FindObjectOfType<AudioManager>().Play("LevelTheme");
            SceneManager.LoadScene(3);
        }
    }
    public void Stage04() {
        if (enterName.text != string.Empty) {
            string newName = enterName.text;
            preferencias.setPlayerName(newName);
            FindObjectOfType<AudioManager>().Stop("MenuTheme");
            FindObjectOfType<AudioManager>().Play("LevelTheme");
            SceneManager.LoadScene(4);
        }
    }


}


