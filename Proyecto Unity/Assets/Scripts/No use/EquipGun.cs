﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipGun : MonoBehaviour
{
    float throwForce = 1500;
    Vector3 objectPos;
    float distance;

    public bool canHold = true;
    public GameObject gun;
    public GameObject tempParent;
    public bool isHolding = false;

    public float speed = 1f;
    public float posSpeed = 2f;


    // Update is called once per frame
    void Update() {

        distance = Vector3.Distance(gun.transform.position, tempParent.transform.position);

        if (Input.GetKeyDown(KeyCode.E) && distance <= 2f) {
            isHolding = true;
            gun.transform.localRotation = tempParent.transform.localRotation;
            gun.transform.localPosition = tempParent.transform.localPosition;
            /*gun.transform.localRotation = Quaternion.Slerp(gun.transform.localRotation, tempParent.transform.localRotation, Time.deltaTime * speed);
            gun.transform.localPosition = Vector3.SmoothDamp(gun.transform.localPosition, tempParent.transform.localPosition, objectPos.velocity);*/
            gun.GetComponent<Rigidbody>().useGravity = false;
            gun.GetComponent<Rigidbody>().detectCollisions = true;
        }
        /*if (distance > 1f) {
            isHolding = false;
        }*/

        //Comprobar if isHolding
        if (isHolding == true) {
            gun.transform.localRotation = tempParent.transform.localRotation;
            gun.transform.localPosition = tempParent.transform.localPosition;
            gun.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gun.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            gun.transform.SetParent(tempParent.transform);

            if (Input.GetKeyDown(KeyCode.Q)) {
                gun.GetComponent<Rigidbody>().AddForce(tempParent.transform.forward * throwForce);
                isHolding = false;
            }
        }
        else {
            objectPos = gun.transform.position;
            gun.transform.SetParent(null);
            gun.GetComponent<Rigidbody>().useGravity = true;
            gun.transform.position = objectPos;
        }
    }
}
