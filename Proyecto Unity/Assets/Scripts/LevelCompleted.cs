﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelCompleted : MonoBehaviour
{

    public GameObject winLevelUI;
    public TMP_Text chronometer;
    
    public void WinPhase() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        winLevelUI.SetActive(true);
        chronometer.GetComponent<TMP_Text>().fontSize = 100f;

    }
}
