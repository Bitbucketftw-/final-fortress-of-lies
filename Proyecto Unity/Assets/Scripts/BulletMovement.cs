﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed;
    Rigidbody rb;

    //public AudioManager soundEffect;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        FindObjectOfType<AudioManager>().Play("EnergyGunShot");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            BodyPartScript bp = collision.gameObject.GetComponent<BodyPartScript>();

            if (!bp.enemy.dead)
                Instantiate(WeaponManager.instance.hitParticlePrefab, transform.position, transform.rotation);

            bp.HidePartAndReplace();
            bp.enemy.Ragdoll();
        }

        if (collision.gameObject.CompareTag("Player")) {
            Debug.Log("HIT");
            HealthManagement health = collision.gameObject.GetComponent<HealthManagement>();
            health.bulletDamage(21);
        }
        Destroy(gameObject);

    }
}
