﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[SelectionBase]
public class EnemyScript : MonoBehaviour
{
    Animator anim;
    public bool dead;
    public Transform weaponHolder;
    public SceneController sceneController;

    public float walkRadius = 70f;
    public float shootRadius = 30f;
    Transform target;
    NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        anim = GetComponent<Animator>();
        //StartCoroutine(RandomAnimation());

        if (weaponHolder.GetComponentInChildren<WeaponScript>() != null)
            weaponHolder.GetComponentInChildren<WeaponScript>().active = false;

    }

    void Update()
    {

        if (!dead) {

            target = Camera.main.transform;
            float distance = Vector3.Distance(target.position, transform.position);
            if (distance <= walkRadius) {
                transform.LookAt(new Vector3(Camera.main.transform.position.x, 0, Camera.main.transform.position.z));
                agent.SetDestination(target.position);
                agent.isStopped = false;
                anim.SetBool("walk", true);
                anim.SetBool("shooting", false);

                if (distance < shootRadius) {
                    agent.isStopped = true;
                    anim.SetBool("shooting", true);
                }
            }
            else {
                anim.SetBool("walk", false);
                agent.isStopped = true;
            }
            

            
        }
            


    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, walkRadius);
        Gizmos.DrawWireSphere(transform.position, shootRadius);
    }

    public void Ragdoll()
    {
        anim.enabled = false;
        BodyPartScript[] parts = GetComponentsInChildren<BodyPartScript>();
        foreach (BodyPartScript bp in parts)
        {
            bp.rb.isKinematic = false;
            bp.rb.interpolation = RigidbodyInterpolation.Interpolate;
        }

        if (!dead) 
        {
            sceneController.enemyDead();
        }
        dead = true;
        
        if (weaponHolder.GetComponentInChildren<WeaponScript>() != null)
        {
            WeaponScript w = weaponHolder.GetComponentInChildren<WeaponScript>();
            w.Release();

        }

    }

    public void Shoot()
    {
        if (dead)
            return;

        if (weaponHolder.GetComponentInChildren<WeaponScript>() != null)
            weaponHolder.GetComponentInChildren<WeaponScript>().Shoot(GetComponentInChildren<ParticleSystem>().transform.position, transform.rotation, true);
            //weaponHolder.GetComponentInChildren<WeaponScript>().Shoot(GetComponentInChildren<BoxCollider>().transform.position, transform.rotation, true);
    }

    /*IEnumerator RandomAnimation()
    {
        anim.enabled = false;
        yield return new WaitForSecondsRealtime(Random.Range(.1f, .5f));
        anim.enabled = true;

    }*/
}
